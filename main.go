package main

import (
	"encoding/csv"
	"fmt"
	"log"
	"strings"
)

func main() {

	data := `string,int,float,bool,date,slice
text1,100,100.23,false,2020-01-01,abc@def.com;ghi@jkl.com;mno@pqr.com
text2,2,3004.34,true,2023-11-30,a@b.com `

	reader := csv.NewReader(strings.NewReader(data))
	rows, err := reader.ReadAll()
	if err != nil {
		log.Fatal(err)
	}
	for _, row := range rows {

		fmt.Printf("%v\n", strings.Join(row, "|"))
	}
}
