package seaswe

import (
	"errors"
	"fmt"
	"reflect"
	"strings"
	"time"
)

func Marshal(v any) ([][]string, error) {
	//check if input is a slice
	argType := reflect.TypeOf(v)
	if argType.Kind() != reflect.Slice {
		return nil, errors.New("input needs to be a slice")
	}

	//check input is a slice of Struct
	sliceType := argType.Elem()
	if sliceType.Kind() != reflect.Struct {
		return nil, errors.New("input needs to be slice of struct")
	}

	header := marshalHeader(sliceType)
	fmt.Println(header)

	var result [][]string
	result = append(result, header)

	sliceVal := reflect.ValueOf(v)
	for i := 0; i < sliceVal.Len(); i++ {
		row, err := marshalRow(sliceVal.Index(i))
		if err != nil {
			return nil, err
		}
		result = append(result, row)
	}
	return result, nil
}

func marshalRow(rowVal reflect.Value) ([]string, error) {
	var row []string
	rowType := rowVal.Type()
	for i := 0; i < rowType.NumField(); i++ {
		colVal := rowVal.Field(i)
		if _, ok := rowType.Field(i).Tag.Lookup("seaswe"); !ok {
			continue
		}
		str, err := getStrValue(colVal, rowType.Field(i).Tag)
		if err != nil {
			return row, err
		}
		row = append(row, str)
	}
	return row, nil
}

func getStrValue(colVal reflect.Value, tag reflect.StructTag) (string, error) {

	colType := colVal.Type()

	switch colType.Kind() {
	case reflect.String:
		return fmt.Sprintf("%s", colVal.String()), nil

	case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64, reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64:
		return fmt.Sprintf("%d", colVal.Int()), nil

	case reflect.Bool:
		return fmt.Sprintf("%t", colVal.Bool()), nil

	case reflect.Float32, reflect.Float64:
		return fmt.Sprintf("%v", colVal.Float()), nil

	case reflect.Slice:
		delimiter, ok := tag.Lookup("delimiter")
		if !ok {
			delimiter = SLICE_DELIMITER
		}
		var x []string
		for i := 0; i < colVal.Len(); i++ {
			s := fmt.Sprintf("%v", colVal.Index(i))
			x = append(x, s)
		}
		return strings.Join(x, delimiter), nil

	case reflect.Struct:
		switch colType.Name() {
		case "Time":
			format, ok := tag.Lookup("format")
			if !ok {
				format = DATE_FORMAT
			}
			t := colVal.Interface().(time.Time)
			return t.Format(format), nil
		default:
			return fmt.Sprintf("%v", colVal), nil
		}
	case reflect.Ptr:
		return getStrValue(colVal.Elem(), tag)

	}
	return "", fmt.Errorf("not a valid type - %v", colVal.Kind())
}

func marshalHeader(t reflect.Type) []string {
	header := []string{}
	for i := 0; i < t.NumField(); i++ {
		field := t.Field(i)
		if _, ok := field.Tag.Lookup("seaswe"); !ok {
			continue
		}
		if fieldTag, ok := field.Tag.Lookup("seaswe"); ok {
			header = append(header, fieldTag)
		}
	}
	return header
}
