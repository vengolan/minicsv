package seaswe

import (
	"fmt"
	"strconv"
	"strings"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

type InnerStruct struct {
	I  int
	S  string
	SL []string
}

func (is *InnerStruct) UnmarshalSEASWE(s string) error {
	fmt.Println("Unmarshalling ", s)
	sarr := strings.Split(s, ":")
	intval, err := strconv.ParseInt(sarr[0], 10, 64)
	if err != nil {
		return fmt.Errorf("Unable to convert %s to Integer while unmarshalling InnerStruct(%s)", sarr[0], s)
	}

	strarray := strings.Split(sarr[2], "\t")

	x := InnerStruct{I: int(intval), S: sarr[1], SL: strarray}
	*is = x
	fmt.Println("complete", is)
	return nil

}
func (is *InnerStruct) MarshalSEASWE() (string, error) {
	s := fmt.Sprintf("%d:%s:%s", is.I, is.S, strings.Join(is.SL, "\t"))
	return s, nil
}

type TestStruct struct {
	S           string      `seaswe:"str_head"`
	I           int         `seaswe:"int_head"`
	B           bool        `seaswe:"bool_head"`
	F           float32     `seaswe:"float_head"`
	SL          []string    `seaswe:"slice_head" delimiter:";"`
	T           time.Time   `seaswe:"time_head" format:"2006-01-02"`
	INNERSTRUCT InnerStruct `seaswe:"struct_head"`
}

func TestUnmarshal(t *testing.T) {
	type args struct {
		data [][]string
		v    any
	}
	tests := []struct {
		name            string
		constructArgs   func() ([][]string, any)
		validateResults func(*testing.T, any, error)
	}{
		//Test cases for Unmarshaller
		{
			name: "#1a - Input Validation - Ensure result type is a valid Pointer",
			constructArgs: func() ([][]string, any) {
				var input struct{}
				return [][]string{}, input
			},
			validateResults: func(t *testing.T, v any, err error) {
				if err != nil && err.Error() != "input should be a pointer(to a slice of structs)" {
					t.Errorf("expecting error - input should be a pointer(to a slice of structs)")
				}
			},
		}, //end of test case 1a
		{
			name: "#1b - Input Validation - Ensure result type a pointer to a slice of Structs",
			constructArgs: func() ([][]string, any) {
				var input struct{}
				return [][]string{}, &input
			},
			validateResults: func(t *testing.T, v any, err error) {
				if err != nil && err.Error() != "input pointer should point to a slice(of structs)" {
					t.Errorf("expecting error - input pointer should point to a slice(of structs)")
				}
			},
		}, //end of test case 1b
		{
			name: "#1c - Input Validation - Ensure the pointer slice contains only Struct values",
			constructArgs: func() ([][]string, any) {
				input := []int{1, 2, 4}
				return [][]string{}, &input
			},
			validateResults: func(t *testing.T, v any, err error) {
				if err != nil && err.Error() != "input slice pointer should only contain struct values" {
					t.Errorf("expecting error - input slice pointer should only contain struct values")
				}
			},
		}, //end of test case 1c
		{
			name: "#2 - Check Initial Values ",
			constructArgs: func() ([][]string, any) {

				val := []TestStruct{}
				input := [][]string{
					{"str_head", "int_head", "bool_head", "float_head", "slice_head", "int_slice_head", "time_head", "struct_head"},
					{"string1", "100", "false", "22.33", "abc;def;ghi", "1;3;4;5", "2023-12-30", "10:innerstr:sl1\tsl2\tsl4"},
					{"string2", "200", "true", "99.382", "xyz;pqr;mno;rst", "67;66;99", "2023-04-30", "10:innerstr2:slice01\tslice04\tslice994"},
				}

				return input, &val
			},

			validateResults: func(t *testing.T, v any, err error) {
				assert.NoError(t, err)
				t1, _ := time.Parse("2006-01-02", "2023-12-30")
				t2, _ := time.Parse("2006-01-02", "2023-04-30")
				inner1 := InnerStruct{I: 10, S: "innerstr", SL: []string{"sl1", "sl2", "sl4"}}
				inner2 := InnerStruct{I: 10, S: "innerstr2", SL: []string{"slice01", "slice04", "slice994"}}
				expected := []TestStruct{
					{"string1", 100, false, 22.33, []string{"abc", "def", "ghi"}, t1, inner1},
					{"string2", 200, true, 99.382, []string{"xyz", "pqr", "mno", "rst"}, t2, inner2},
				}
				got, ok := v.(*[]TestStruct)
				fmt.Printf("\tgot:%v\n\texp:%v\n", got, expected)
				if !ok {
					t.Errorf("unable to convert result to *[]TestStruct")
				}
				assert.ElementsMatch(t, expected, *got)

			},
		}, //end of test case 1c
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {

			input, v := tt.constructArgs()
			err := Unmarshal(input, v)
			tt.validateResults(t, v, err)

		})
	}
}
