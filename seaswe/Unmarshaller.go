package seaswe

import (
	"fmt"
	"reflect"
	"strconv"
	"strings"
	"time"
)

// Unmarshal - takes string array as input and expects a []Struct value
func Unmarshal(data [][]string, sliceptr any) error {

	//ensure v is a pointer to an array of Struct values
	ptrType := reflect.TypeOf(sliceptr)
	if ptrType.Kind() != reflect.Ptr {
		return fmt.Errorf("input should be a pointer(to a slice of structs)")
	}

	sliceType := ptrType.Elem()
	if sliceType.Kind() != reflect.Slice {
		return fmt.Errorf("input pointer should point to a slice(of structs)")
	}

	structType := sliceType.Elem()
	if structType.Kind() != reflect.Struct {
		return fmt.Errorf("input slice pointer should only contain struct values")
	}

	//first row is header
	header := data[0]
	fieldPos := make(map[string]int, len(header))
	for i, h := range header {
		fieldPos[h] = i
	}

	//use Elem() since v is a pointer to the slice
	sliceval := reflect.ValueOf(sliceptr).Elem()

	for _, row := range data[1:] {
		structVal := reflect.New(structType).Elem() //Always need to get the acutual element using the .Elem method to set the value
		err := unmarshallRow(row, fieldPos, structVal)
		if err != nil {
			return err
		}
		sliceval.Set(reflect.Append(sliceval, structVal))
	}

	return nil
}

func unmarshallRow(row []string, fieldPos map[string]int, val reflect.Value) error {
	valType := val.Type()
	for i := 0; i < val.NumField(); i++ {
		colVal := val.Field(i)
		fieldTag := valType.Field(i).Tag
		tag, ok := fieldTag.Lookup("seaswe")
		if !ok {
			continue
		}
		pos, ok := fieldPos[tag]
		if !ok {
			continue
		}

		strVal := row[pos]
		var isPtr bool
		var colValPtr reflect.Value

		if colVal.Kind() == reflect.Ptr {
			fmt.Println("********************", colVal.Elem())
			isPtr = true
			colValPtr = colVal
			colVal = colValPtr.Elem()
		}
		err := convertStrValue(colVal, strVal, fieldTag)
		if isPtr {
			colValPtr.Set(colVal.Addr())
		}

		if err != nil {
			return fmt.Errorf("error while converting %s: %w", strVal, err)
		}
	}
	return nil
}

func convertStrValue(colVal reflect.Value, strVal string, tag reflect.StructTag) error {
	switch colVal.Kind() {
	case reflect.Int:
		i, err := strconv.ParseInt(strVal, 10, 64)
		if err != nil {
			return fmt.Errorf("got error while trying to convert %s to int:%w", strVal, err)
		}
		colVal.SetInt(i)
	case reflect.String:
		colVal.SetString(strVal)

	case reflect.Bool:
		b, err := strconv.ParseBool(strVal)
		if err != nil {
			return fmt.Errorf("got error while trying to convert %s to bool:%w", strVal, err)
		}
		colVal.SetBool(b)

	case reflect.Float32, reflect.Float64:
		b, err := strconv.ParseFloat(strVal, 64)
		if err != nil {
			return fmt.Errorf("got error while trying to convert %s to float:%w", strVal, err)
		}
		colVal.SetFloat(b)

	case reflect.Struct:
		switch colVal.Type().Name() {
		case "Time":
			format, ok := tag.Lookup("format")
			if !ok {
				format = DATE_FORMAT
			}
			t, err := time.Parse(format, strVal)
			if err != nil {
				return fmt.Errorf("got error while converting %s to timestamp(%s):%w", strVal, format, err)
			}
			timeVal := reflect.ValueOf(t)
			colVal.Set(timeVal)

		case "InnerStruct": //all other struct types other than Time
			inter := colVal.Addr().Interface()
			xxx, ok := inter.(SEASWEMarshaller)
			fmt.Println("*************", xxx, ok)
			if ok {
				e := xxx.UnmarshalSEASWE(strVal)
				fmt.Println("$$$$$$$$$$$$$$$$$$$$$", xxx, e)
				xval := reflect.ValueOf(xxx).Elem()
				colVal.Set(xval)
				return e
			}
		default:
			return fmt.Errorf("cannot handle field of type %v - %v", colVal.Type().Name(), colVal.Kind())
		}

	case reflect.Slice:
		delimiter, ok := tag.Lookup("delimiter")
		if !ok {
			delimiter = SLICE_DELIMITER
		}
		sliceType := colVal.Type().Elem()
		strArr := strings.Split(strVal, delimiter)
		for _, str := range strArr {
			sliceElementVal := reflect.New(sliceType).Elem()
			convertStrValue(sliceElementVal, str, tag)
			colVal.Set(reflect.Append(colVal, sliceElementVal))
		}

	default:
		return fmt.Errorf("cannot handle field of type %v - %v", colVal.Type().Name(), colVal.Kind())
	}
	return nil
}
