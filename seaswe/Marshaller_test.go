package seaswe

import (
	"fmt"
	"reflect"
	"testing"
	"time"
)

type Person struct {
	Name string
	Age  int
}

func (p Person) String() string {
	return fmt.Sprintf("%s(%d years)", p.Name, p.Age)
}

type InputStruct struct {
	S  string `seaswe:"string_head"`
	X  int
	I  int       `seaswe:"int_head"`
	F  float64   `seaswe:"float_head"`
	B  bool      `seaswe:"bool_head"`
	DT time.Time `seaswe:"date_head" format:"2006-01-02"`
	SL []string  `seaswe:"slice_head" delimiter:";"`
}

var expected_header = []string{"string_head", "int_head", "float_head", "bool_head", "date_head", "slice_head"}

func TestMarshal(t *testing.T) {
	type args struct {
		v any
	}
	tests := []struct {
		name            string
		constructArgs   func() any
		validateResults func(*testing.T, [][]string, error)
	}{
		//
		// Test No 1
		{
			name: "#1. Non Slice Input",
			constructArgs: func() any {
				return InputStruct{}
			},
			validateResults: func(t *testing.T, got [][]string, err error) {
				t.Helper()
				if err.Error() != "input needs to be a slice" {
					t.Errorf("expecting error - input needs to be a slice")
				}
			},
		},

		/// Test No 2
		{
			name: "#2.Slice of Non Structs",
			constructArgs: func() any {
				return []string{"asdfasdf", "Asdfasdf"}
			},
			validateResults: func(t *testing.T, s [][]string, err error) {
				t.Helper()
				fmt.Println(err.Error())
				if err.Error() != "input needs to be slice of struct" {
					t.Errorf("expecting error - input needs to be slice of struct")
				}
			},
		},

		//Test No 3
		{
			name: "#3. Check Header",
			constructArgs: func() any {
				return []InputStruct{}
			},
			validateResults: func(t *testing.T, s [][]string, err error) {
				t.Helper()
				if err != nil {
					t.Errorf("unexpected error %v", err)
				}
				header := s[0]
				result := reflect.DeepEqual(header, expected_header)
				if !result {
					t.Errorf("\nexp:%v\ngot:%v\n", header, expected_header)
				}
			},
		},

		//Test No 4
		{
			name: "#4. Check Basic Types",
			constructArgs: func() any {
				t, _ := time.Parse("2006-01-02", "2023-12-15")

				return []InputStruct{
					{
						S:  "teststr",
						X:  10,
						I:  2304994,
						F:  2,
						B:  false,
						DT: t,
						SL: []string{"one", "two", "three"},
					},
				}
			},
			validateResults: func(t *testing.T, s [][]string, err error) {
				t.Helper()
				if err != nil {
					t.Errorf("unexpected error %v", err)
				}

				expected := [][]string{
					expected_header,
					{"teststr", "2304994", "2", "false", "2023-12-15", "one;two;three"},
				}

				if !reflect.DeepEqual(s, expected) {
					t.Errorf("\n\ngot:%v\nexpected:%v", s, expected)
				}

			},
		}, //ond of Test #4,
		/////////////////////////////////////

		//TEST #5
		{
			name: "#6. Check Pointer Values",
			constructArgs: func() any {
				s := "teststr"
				i := int64(2304994)
				f := float32(2.0)
				b := false
				dt, _ := time.Parse("2006-01-02", "2023-12-15")
				sl := []string{"one", "two", "three"}

				type test struct {
					S  *string    `seaswe:"string_head"`
					I  *int64     `seaswe:"int_head"`
					F  *float32   `seaswe:"float_head"`
					B  *bool      `seaswe:"bool_head"`
					DT *time.Time `seaswe:"date_head" format:"2006-01-02"`
					SL *[]string  `seaswe:"slice_head" delimiter:";"`
				}

				return []test{
					{
						S:  &s,
						I:  &i,
						F:  &f,
						B:  &b,
						DT: &dt,
						SL: &sl,
					},
				}
			},

			validateResults: func(t *testing.T, s [][]string, err error) {
				t.Helper()
				if err != nil {
					t.Errorf("unexpected error %v", err)
				}

				expected := [][]string{
					expected_header,
					{"teststr", "2304994", "2", "false", "2023-12-15", "one;two;three"},
				}
				if !reflect.DeepEqual(s, expected) {
					t.Errorf("\n\ngot:%v\nexpected:%v", s, expected)
				}

			},
		},

		//Test #7: check different slice values
		{
			name: "#7. check different slice types",
			constructArgs: func() any {
				s := "teststr"
				i := int64(2304994)
				f := float32(2.0)
				b := false
				dt, _ := time.Parse("2006-01-02", "2023-12-15")
				sl := []int{1, 2, 3}

				type test struct {
					S  *string    `seaswe:"string_head"`
					I  *int64     `seaswe:"int_head"`
					F  *float32   `seaswe:"float_head"`
					B  *bool      `seaswe:"bool_head"`
					DT *time.Time `seaswe:"date_head" format:"2006-01-02"`
					SL *[]int     `seaswe:"slice_head" delimiter:";"`
				}

				return []test{
					{
						S:  &s,
						I:  &i,
						F:  &f,
						B:  &b,
						DT: &dt,
						SL: &sl,
					},
				}
			},

			validateResults: func(t *testing.T, s [][]string, err error) {
				t.Helper()
				if err != nil {
					t.Errorf("unexpected error %v", err)
				}

				expected := [][]string{
					expected_header,
					{"teststr", "2304994", "2", "false", "2023-12-15", "1;2;3"},
				}
				if !reflect.DeepEqual(s, expected) {
					t.Errorf("\n\ngot:%v\nexp:%v", s, expected)
				}
			},
		},
		// Test #8: Check Inner struct Values
		{
			name: "#8. Check inner struct values",
			constructArgs: func() any {
				s := "teststr"
				i := int64(2304994)
				f := float32(2.0)
				b := false
				dt, _ := time.Parse("2006-01-02", "2023-12-15")
				sl := []int{1, 2, 3}

				type test struct {
					S       *string    `seaswe:"string_head"`
					I       *int64     `seaswe:"int_head"`
					F       *float32   `seaswe:"float_head"`
					B       *bool      `seaswe:"bool_head"`
					DT      *time.Time `seaswe:"date_head" format:"2006-01-02"`
					SL      *[]int     `seaswe:"slice_head" delimiter:";"`
					ISTRUCT *[]*Person `seaswe:"struct_head" delimiter:"&"`
				}

				return []test{
					{
						S:  &s,
						I:  &i,
						F:  &f,
						B:  &b,
						DT: &dt,
						SL: &sl,
						ISTRUCT: &[]*Person{
							{Name: "Name1", Age: 1000},
							{Name: "Name2", Age: 500},
						},
					},
				}
			},

			validateResults: func(t *testing.T, s [][]string, err error) {
				t.Helper()
				if err != nil {
					t.Errorf("unexpected error %v", err)
				}

				expected := [][]string{
					{"string_head", "int_head", "float_head", "bool_head", "date_head", "slice_head", "struct_head"},
					{"teststr", "2304994", "2", "false", "2023-12-15", "1;2;3", "Name1(1000 years)&Name2(500 years)"},
				}
				if !reflect.DeepEqual(s, expected) {
					t.Errorf("\n\ngot:%v\nexp:%v", s, expected)
				}
			},
		},
	} //end of test case array

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			x := tt.constructArgs()
			got, err := Marshal(x)
			tt.validateResults(t, got, err)
		})
	}
}
