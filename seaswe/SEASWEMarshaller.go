package seaswe

type SEASWEMarshaller interface {
	MarshalSEASWE() (string, error)
	UnmarshalSEASWE(string) error
}
